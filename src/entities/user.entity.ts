import { Column, Entity } from 'typeorm';
import { Roles } from '../shared/enums/user-roles';
import { BaseEntity } from './base.entity';

@Entity()
export class User extends BaseEntity {
  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ unique: true })
  email: string;

  @Column({ select: false })
  password: string;

  @Column({ default: Roles.USER })
  role: Roles;
}
