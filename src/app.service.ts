import { Injectable } from '@nestjs/common';
import { config } from 'dotenv';

@Injectable()
export class AppService {

  async getHello() {
    config();
    return 'hey';
  }
}
